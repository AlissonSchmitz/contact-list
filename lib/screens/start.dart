import 'package:contact_list/screens/address_user_map.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'contact_list.dart';

class Start extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final pageViewController = PageController();

    @override
    void dispose() {
      pageViewController.dispose();
    }

    return Scaffold(
      body: PageView(
        controller: pageViewController,
        physics: new NeverScrollableScrollPhysics(),
        children: [
          ContactList(),
          AdressUserMap(),
        ],
      ),
      bottomNavigationBar: AnimatedBuilder(
          animation: pageViewController,
          builder: (context, snapshot) {
            return BottomNavigationBar(
              showUnselectedLabels: false,
              currentIndex: pageViewController?.page?.round() ?? 0,
              onTap: (index) {
                pageViewController.animateToPage(index,
                    duration: Duration(milliseconds: 300),
                    curve: Curves.decelerate);
              },
              selectedItemColor: Theme.of(context).primaryColor,
              items: const <BottomNavigationBarItem>[
                BottomNavigationBarItem(
                  icon: Icon(Icons.contacts),
                  label: 'Contacts',
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.map),
                  label: 'Map',
                ),
              ],
            );
          }),
    );
  }
}
