import 'package:contact_list/database/daos/contact_dao.dart';
import 'package:contact_list/models/user_contact.dart';
import 'package:contact_list/screens/contact_form.dart';
import 'package:contact_list/screens/widgets/contact_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ContactList extends StatelessWidget {
  ContactDao contactDao = new ContactDao();
  List<UserContact> _userList;

  ContactList(){

  }

  _init() async {
    return _userList = await contactDao.getAll();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Contacts'),
      ),
      body: FutureBuilder(
          future: _init(),
          builder: (context, AsyncSnapshot snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return Center(
                child: CircularProgressIndicator(),
              );
            } else if(snapshot.connectionState == ConnectionState.done) {
                return ListView.builder(
                    itemCount: _userList.length,
                    itemBuilder: (BuildContext context, int index) {
                      return ContactWidget(_userList[index]);
                    });
              }else{
             return Container();
            }
          }),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.person_add_alt_1),
        onPressed: (){
          Navigator.push(context, CupertinoPageRoute(builder: (context) => ContactForm()));
        },
      ),
    );
  }
}
