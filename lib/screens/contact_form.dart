import 'package:contact_list/database/daos/contact_dao.dart';
import 'package:contact_list/models/user_contact.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ContactForm extends StatelessWidget {
  ContactDao _contactDao = new ContactDao();
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _surnameController = TextEditingController();
  final TextEditingController _phoneController = TextEditingController();

  Widget inputFormText(String label, TextEditingController controller) {
    return Padding(
      padding: EdgeInsets.all(4),
      child: TextField(
        controller: controller,
        decoration: InputDecoration(
          border: OutlineInputBorder(),
          labelText: label,
        ),
      ),
    );
  }

  Widget inputFormNumber(String label, TextEditingController controller) {
    return Padding(
      padding: EdgeInsets.all(4),
      child: TextField(
        controller: controller,
        decoration: InputDecoration(
          border: OutlineInputBorder(),
          labelText: label,
        ),
        keyboardType: TextInputType.number,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('New contact'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: <Widget>[
            inputFormText('Name', _nameController),
            inputFormText('Surname', _surnameController),
            inputFormNumber('Phone', _phoneController),
            Padding(
              padding: const EdgeInsets.only(top: 16.0),
              child: SizedBox(
                width: double.maxFinite,
                child: RaisedButton(
                  color: Theme.of(context).primaryColor,
                  textColor: Colors.white,
                  onPressed: () {
                    final String name = _nameController.text;
                    final String surname = _surnameController.text;
                    final String phone = _phoneController.text;
                    if (name == '' || surname == '' || phone == '') {
                      showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return AlertDialog(
                              title: Text('Warning'),
                              content: Text('You must fill all the fields'),
                              actions: [
                                FlatButton(onPressed: (){
                                  Navigator.pop(context);
                                }, child: Text('CONFIRM')),
                              ],
                            );
                          });
                    } else {
                      final UserContact userContact =
                          new UserContact(0, name, surname, phone, '', '');
                      _contactDao.saveContact(userContact);
                      Navigator.pop(context);
                    }
                  },
                  child: Text('SAVE'),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
