import 'package:contact_list/models/user_location.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';

class AdressUserMap extends StatelessWidget {
  UserLocation userLocation;
  Location location;
  GoogleMapController mapController;
  Set<Marker> markers = new Set<Marker>();
  double latitude;
  double longitude;
  LatLng position;

  AdressUserMap() {
    _init();
  }

  _init() async {
    location = new Location();
    await _serviceIsEnable();
    userLocation = await _getLocation();
    print(userLocation.latitude);
    print(userLocation.longitude);
    position = LatLng(userLocation.latitude, userLocation.longitude);
    final Marker marker = Marker(
        markerId: new MarkerId("123456"),
        position: position,
        infoWindow: InfoWindow(
          snippet: "Local atual",
          title: "Titulo aqui",
        ));
    markers.add(marker);
  }

  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Testing google maps'),
        //title: TextFormField(),
      ),
      body: GoogleMap(
        onMapCreated: _onMapCreated,
        zoomGesturesEnabled: true,
        initialCameraPosition: CameraPosition(
            target: LatLng(-26.9388081, -48.9360102), zoom: 15.0),
        markers: markers,
        onTap: (data) {
          print(data);
        },
      ),
    );
  }

  Future<UserLocation> _getLocation() async {
    if (await _serviceIsPermission()) {
      var teste = await location.getLocation();
      UserLocation _userLocation;
      try {
        _userLocation = new UserLocation(
          teste.latitude,
          teste.longitude,
          teste.speedAccuracy,
          teste.altitude,
          teste.speed,
          teste.accuracy,
          teste.time,
          teste.heading,
        );
        return _userLocation;
      } catch (e) {
        print('Could not get the location $e');
      }
    }
    return null;
  }

  _serviceIsEnable() async {
    var _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return false;
      }
    }
    return true;
  }

  Future<bool> _serviceIsPermission() async {
    var _servicePermission = await location.hasPermission();
    if (_servicePermission == PermissionStatus.DENIED) {
      _servicePermission = await location.requestPermission();
      if (_servicePermission != PermissionStatus.GRANTED) {
        return false;
      }
    }
    return true;
  }
}
