import 'package:contact_list/database/daos/contact_dao.dart';
import 'package:contact_list/models/user_contact.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ContactWidget extends StatelessWidget {
  final ContactDao contactDao = new ContactDao();
  UserContact _userContact;
  String _name;
  String _surname;
  String _phone;
  String _avatarText;

  ContactWidget(UserContact contact) {
    _userContact = contact;
    _name = contact.name;
    _surname = contact.surname;
    _phone = contact.phone;
    _avatarText = '${_name.substring(0, 1)}${_surname.substring(0, 1)}';
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        leading: CircleAvatar(
          child: Text('$_avatarText'),
        ),
        title: Text('$_name $_surname'),
        subtitle: Text('$_phone'),
        trailing: Icon(Icons.location_on_rounded),
        onLongPress: () {
          return showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  //shape: CircleBorder(),
                  title: Text('Delete Contact'),
                  content:
                      Text('Do you reallt want to delete the $_name contact?'),
                  actions: [
                    FlatButton(
                      child: Text('Yes'),
                      onPressed: () async{
                        await contactDao.deleteContact(_userContact);
                        Navigator.pop(context);
                      },
                    ),
                    FlatButton(
                      child: Text('No'),
                      onPressed: (){
                        Navigator.pop(context);
                      },
                    ),
                  ],
                );
              });
        },
      ),
    );
  }

//  @override
//  Widget build(BuildContext context) {
//    return Column(
//      children: [
//        ListTile(
//          leading: CircleAvatar(child: Text('$_avatarText'),),
//          title: Text('$_name $_surname'),
//          subtitle: Text('$_phone'),
//          trailing: Icon(Icons.location_on_rounded),
//        ),
//        Divider(
//          indent: 60,
//          thickness: 1.5,
//        ),
//      ],
//    );
//  }
}
