class UserContact{
  final int _id;
  final String _name;
  final String _surname;
  final String _phone;
  final String _latitude;
  final String _longitude;

  UserContact(this._id, this._name, this._surname, this._phone, this._latitude, this._longitude);

  int get id => _id;

  String get name => _name;

  String get surname => _surname;

  String get phone => _phone;

  String get latitude => _latitude;

  String get longitude => _longitude;

  Map<String, dynamic> toMap(){
    return {
      'name': _name,
      'surname': _surname,
      'phone' : _phone,
      'latitude' : _latitude,
      'longitude': _longitude
    };
  }
}