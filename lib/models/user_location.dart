class UserLocation{
  double _latitude;
  double _longitude;
  double _speecAccuracy;
  double _altitude;
  double _speed;
  double _accuracy;
  double _time;
  double _heading;

  UserLocation(this._latitude, this._longitude, this._speecAccuracy,
      this._altitude, this._speed, this._accuracy, this._time, this._heading);

  double get heading => _heading;

  set heading(double value) {
    _heading = value;
  }

  double get time => _time;

  set time(double value) {
    _time = value;
  }

  double get accuracy => _accuracy;

  set accuracy(double value) {
    _accuracy = value;
  }

  double get speed => _speed;

  set speed(double value) {
    _speed = value;
  }

  double get altitude => _altitude;

  set altitude(double value) {
    _altitude = value;
  }

  double get speecAccuracy => _speecAccuracy;

  set speecAccuracy(double value) {
    _speecAccuracy = value;
  }

  double get longitude => _longitude;

  set longitude(double value) {
    _longitude = value;
  }

  double get latitude => _latitude;

  set latitude(double value) {
    _latitude = value;
  }
}
