import 'package:contact_list/database/daos/contact_dao.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

Future<Database> getDatabase() async {
  final String path = join(await getDatabasesPath(), 'contact_list.db');
  return openDatabase(
    path,
    onCreate: (db, version) {
      db.execute(ContactDao.tableContactSql); // posso fazer vários EXECUTES a partir de cada DAO que é criado para cada "widget"
    },
    onConfigure: _onConfigure,
    version: 1,
  );
}

Future _onConfigure(Database db) async {
  await db.execute('PRAGMA foreign_keys = ON');
}
