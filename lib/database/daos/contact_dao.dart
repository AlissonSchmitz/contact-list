import 'package:contact_list/models/user_contact.dart';
import 'package:sqflite/sqflite.dart';

import '../app_database.dart';

class ContactDao {
  static const String _tableName = 'contacts';
  static const String _id = 'id';
  static const String _name = 'name';
  static const String _surname = 'surname';
  static const String _phone = 'phone';
  static const String _latitude = 'latitude';
  static const String _longitude = 'longitude';

  static const String tableContactSql = 'CREATE TABLE $_tableName ('
      '$_id INTEGER PRIMARY KEY AUTOINCREMENT, '
      '$_name TEXT, '
      '$_surname TEXT, '
      '$_phone TEXT, '
      '$_latitude TEXT, '
      '$_longitude TEXT);';

  Future<int> saveContact(UserContact contact) async {
    final Database db = await getDatabase();
    return db.insert(_tableName, contact.toMap());
  }

  void deleteContact(UserContact contact) async{
    final Database db = await getDatabase();
    db.delete(_tableName, where: ' $_id = ?', whereArgs: [contact.id]);
  }

  void deleteAll() async{
    final Database db = await getDatabase();
    db.delete(_tableName);
  }

  Future<List<UserContact>> getAll() async {
    final Database db = await getDatabase();
    final List<UserContact> contactList = List();
    final List<Map<String, dynamic>> result = await db.query(_tableName);
    for (Map<String, dynamic> item in result) {
      contactList.add(UserContact(
        item[_id],
        item[_name],
        item[_surname],
        item[_phone],
        item[_latitude],
        item[_longitude]
      ));
    }
    return contactList;
  }
}
